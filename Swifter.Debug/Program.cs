﻿using System;
using System.Net;
using System.Threading.Tasks;

namespace Swifter.Debug
{
    class Program
    {
        static void Main(string[] args)
        {
            // Console.WriteLine("Hello World!");

            for (int i = 0; i < 8; i++)
            {
                Task.Run(Loop);
            }

            static async void Loop()
            {
                while (true)
                {
                    using var response = await WebRequest.Create("http://localhost:8888/?Command=MakeToken&ContentType=Json").GetResponseAsync();
                }
            }

            Console.ReadKey();
        }
    }
}
