﻿using System;

namespace Swifter.Data.Sql
{
    /// <summary>
    /// 表示一个后期获取的值。
    /// </summary>
    /// <typeparam name="T">值类型</typeparam>
    public class LateValue<T> : SqlValue<T>
    {
        private readonly Func<T> Getter;

        /// <summary>
        /// 获取值
        /// </summary>
        public override T Value => Getter();

        internal LateValue(Func<T> getter)
        {
            Getter = getter;
        }
    }
}