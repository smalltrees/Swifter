function SelCity(obj, e) {
    var ths = obj;

    var hProvince = $(ths).parent().find("input[name=HProvinceId]");
    var hCity = $(ths).parent().find("input[name=HCityId]");
    var hDistrict = $(ths).parent().find("input[name=HDistrictId]");

    if (!hProvince.length) {
        hProvince = $('<input>', {
            type: 'hidden',
            name: "HProvinceId",
        });
        $(ths).after(hProvince);
    }
    if (!hCity.length) {
        hCity = $('<input>', {
            type: 'hidden',
            name: "HCityId",
        });
        $(ths).after(hCity);
    }
    if (!hDistrict.length) {
        hDistrict = $('<input>', {
            type: 'hidden',
            name: "HDistrictId",
        });
        $(ths).after(hDistrict);
    }
    
    Iput.show({
        id: ths,
        event: e,
        content: '<div class="_citys"><span title="关闭" id="cColse" >×</span><div id="_citysheng" class="_citys0">请选择省份</div><div id="_citys0" class="_citys1"></div><div style="display:none" id="_citys1" class="_citys1"></div><div style="display:none" id="_citys2" class="_citys1"></div></div>',
        width: "470"
    });

    $("#cColse").click(function() {
        Iput.colse()
    });

    var data = getAreas();

    for (var i in data) {
        var item = data[i];
        if (item.Id && !item.Parent) {
            $("#_citys0").append($("<a data-id='" + item.Id + "' data-name='" + item.Name + "'>" + item.Name + "</a>"));
        }
    }

    $("#_citys0 a").click(function () {
        var citys = data[$(this).data("id")].Items;

        $("#_citysheng").html('请选择城市');

        $("#_citys1 a").remove();

        for (var i in citys) {
            var item = citys[i];
            if (item.Id) {
                $("#_citys1").append($("<a data-id='" + item.Id + "' data-name='" + item.Name + "'>" + item.Name + "</a>"));
            }
        };

        $("#_citys0").hide();
        $("#_citys1").show();

        hProvince.val($(this).data("id")).data("name", $(this).data("name"));
        hCity.val("").data("name", "");
        hDistrict.val("").data("name", "");

        $(ths).val(hProvince.data("name"));
        
        if (!citys) {
            Iput.colse();
        }

        $("#_citys1 a").click(function () {
            var areas = data[$(this).data("id")].Items;

            $("#_citysheng").html('请选择区县');

            $("#_citys2 a").remove();

            for (var i in areas) {
                var item = areas[i];
                if (item.Id) {
                    $("#_citys2").append($("<a data-id='" + item.Id + "' data-name='" + item.Name + "'>" + item.Name + "</a>"));
                }
            };

            $("#_citys1").hide();
            $("#_citys2").show();

            hCity.val($(this).data("id")).data("name", $(this).data("name"));
            hDistrict.val("").data("name", "");

            $(ths).val(hProvince.data("name") + "/" + hCity.data("name"));
            
            if (!areas) {
                Iput.colse();
            }

            $("#_citys2 a").click(function () {

                $("#_citys2 a").removeClass("AreaS");
                $(this).addClass("AreaS");

                hDistrict.val($(this).data("id")).data("name", $(this).data("name"));

                $(ths).val(hProvince.data("name") + "/" + hCity.data("name") + "/" + hDistrict.data("name"));

                Iput.colse()
            })
        })
    })
}
function getCity(obj) {
    var c = obj.data('id');
    var f = getAreas()[c].Items;
    var g = '';
    for (var i in f) {
        var item = f[i];
        if (item.Id) {
            g += '<a data-id="' + item.Id + '" data-name="' + item.Name + '" title="' + item.Name + '">' + item.Name + '</a>'
        }
    }
    $("#_citysheng").html('请选择城市');
    return g
}
function getArea(obj) {
    var c = obj.data('id');
    var f = getAreas()[c].Items;
    var g = '';
    for (var i in f) {
        var item = f[i];
        if (item.Id) {
            g += '<a data-id="' + item.Id + '" data-name="' + item.Name + '" title="' + item.Name + '">' + item.Name + '</a>'
        }
    }
    $("#_citysheng").html('请选择区县');
    return g
}