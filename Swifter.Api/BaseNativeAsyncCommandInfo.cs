﻿using Swifter.RW;
using System;
using System.Threading.Tasks;

#nullable enable

namespace Swifter.Api
{
    public abstract class BaseNativeAsyncCommandInfo<T> : INativeCommandInfo where T : BaseNativeAsyncCommandInfo<T>
    {
        public string Name => typeof(T).Name;

        public abstract Version Version { get; }

        public abstract Task<object?> Invoke(ContextInfo contextInfo, ContextParameters parameters);

        public void Invoke(ContextInfo context, ContextParameters parameters, Action<object?> callback)
        {
            ValueInterface<T>.ReadValue(ValueCopyer.ValueOf(parameters.CommandParameters)).Invoke(context, parameters).ContinueWith((task) =>
            {
                callback(task.Result);
            });
        }
    }
}