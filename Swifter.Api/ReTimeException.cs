﻿namespace Swifter.Api
{
    /// <summary>
    /// 要求客户端调整时间。
    /// </summary>
    public sealed class ReTimeException : BaseException
    {
        public ReTimeException(string message) : base(ReturnCodes.ReTime, message)
        {
        }
    }
}
