﻿using System;

namespace Swifter.Api
{
    public sealed class RuntimeCacheInfo
    {
        public object Data { get; set; }

        public DateTimeOffset CacheTime { get; set; }
    }
}
