﻿using System;

namespace Swifter.Api.Commands
{
    public sealed class Login : BaseNativeSyncCommandInfo<Login>
    {
        public override Version Version { get; } = Versions.Release;

        public string UserName { get; set; }

        public string Password { get; set; }

        public override object Invoke(ContextInfo contextInfo, ContextParameters parameters)
        {
            return new { UserName, UserId = 1 };
        }
    }
}