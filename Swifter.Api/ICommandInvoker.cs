﻿using System;

#nullable enable

namespace Swifter.Api
{
    /// <summary>
    /// 命令执行器信息。
    /// </summary>
    public interface ICommandInvoker
    {
        /// <summary>
        /// 执行该命令。
        /// </summary>
        /// <param name="context">上下文信息</param>
        /// <param name="callback">结果回调函数</param>
        void Invoke(ContextInfo context, ContextParameters parameters, Action<object?> callback);
    }
}
