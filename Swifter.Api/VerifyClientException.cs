﻿namespace Swifter.Api
{
    /// <summary>
    /// 要求验证客户端。
    /// </summary>
    public sealed class VerifyClientException : BaseException
    {
        public VerifyClientException(string message) : base(ReturnCodes.VerifyClient, message)
        {
        }
    }
}
