﻿using Swifter.RW;
using System;
using System.Collections.Generic;

namespace Swifter.Api
{
    public sealed class CommandInfo
    {
        /// <summary>
        /// 命令名称。
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 命令 Id。
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// 命令版本号。
        /// </summary>
        public Version Version { get; set; }

        /// <summary>
        /// 命令的处理器名称。
        /// </summary>
        public string Type { get; set; }

        /// <summary>
        /// 命令标识。
        /// </summary>
        public CommandFlags Flags { get; set; }

        /// <summary>
        /// 命令的缓存键。
        /// </summary>
        public string CacheKey { get; set; }

        /// <summary>
        /// 命令的锁键。
        /// </summary>
        public string LockKey { get; set; }

        /// <summary>
        /// 扩展参数。
        /// </summary>
        public Dictionary<string, ValueCopyer> Extended { get; set; }

        public List<ParameterInfo> Parameters { get; set; }

        public List<ResultInfo> Results { get; set; }

        public sealed class ParameterInfo
        {
            public string Name { get; set; }

            public string Type { get; set; }

            public string DefaultValue { get; set; }

            public string Description { get; set; }
        }

        public sealed class ResultInfo
        {
            public string Name { get; set; }

            public string Type { get; set; }

            public string DemoValue { get; set; }
        }
    }

}
