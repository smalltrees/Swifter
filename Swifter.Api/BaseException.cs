﻿using System;

namespace Swifter.Api
{
    public abstract class BaseException : Exception
    {
        public int Code { get; set; }

        public object ReturnData { get; set; }

        public BaseException(ReturnCodes code, string message) : base(message)
        {
            Code = (int)code;
        }
    }
}
