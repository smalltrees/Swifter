﻿using System;

namespace Swifter.Api
{
    public interface INativeCommandInfo : ICommandInvoker
    {
        string Name { get; }

        Version Version { get; }
    }
}