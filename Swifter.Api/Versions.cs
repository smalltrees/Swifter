﻿using System;

namespace Swifter.Api
{
    public sealed class Versions
    {
        /// <summary>
        /// 绝对发布版本号。
        /// </summary>
        public static readonly Version Release = Version.Parse("0.0.0");

        /// <summary>
        /// 绝对调试版本号。
        /// </summary>
        public static readonly Version Debug = null;
    }
}
